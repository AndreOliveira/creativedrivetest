﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
	[SerializeField] private UnityEngine.UI.Image fill;
	[SerializeField] private string scene;
	private AsyncOperation asyncOp;

	private void Start() {
		asyncOp = SceneManager.LoadSceneAsync(scene);
	}

	private void Update() {
		float progress = (asyncOp.progress >= 0.9f) ? 1f : asyncOp.progress;
		fill.fillAmount = progress;
	}
}
