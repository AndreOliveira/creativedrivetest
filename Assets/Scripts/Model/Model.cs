﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model : MonoBehaviour {

	#region properties
	[SerializeField] private ScriptableSelectedModel selectedModel;

	// start transform values field
	public Vector3 startPosition { get; private set; }
	public Vector3 startScale { get; private set; }
	public Quaternion startRotation { get; private set; }

	// material field
	public Material material;
	public Texture[] textures;
	public Color[] colors;
	public int selectedTexture;
	public int selectedColor;

	// model
	private SerializableModel serializableModel;
	public Rigidbody rb { get; private set; }
	public ScriptableAppEvent onSelectedEvent;
	#endregion

	#region monoebehaviour methods
	private void Start () {
		// setup transform
		startPosition = transform.position;
		startScale = transform.localScale;
		startRotation = transform.rotation;

		//setup physics
		rb = GetComponent<Rigidbody> ();
		if (rb == null) {
			rb = gameObject.AddComponent<Rigidbody> ();
			rb.isKinematic = true;
			rb.useGravity = false;
		}
	}

	private void OnMouseDown () {
		selectedModel.model = this;
		onSelectedEvent.Call ();
	}
	#endregion

	#region public methods
	public void SetupModel (SerializableModel model, ScriptableSelectedModel selectedModel) {
		//setup model and material
		this.selectedModel = selectedModel;
		material = GetComponent<Renderer> ().material;

		//setup colors
		colors = model.colors;
		ChangeColor (model.selectedColor);

		// setup textures
		textures = model.LoadTextures ();
		ChangeTexture (model.selectedTexture);
	}

	public void ChangeColor (int index) {
		selectedColor = index;
		material.SetColor (Constants.ALBEDO_COLOR_KEY, colors[selectedColor]);
	}

	public void ChangeTexture (int index) {
		selectedTexture = index;
		material.SetTexture (Constants.ALBEDO_KEY, textures[selectedTexture]);
	}

	public void ResetModel () {
		rb.MovePosition (startPosition);
		rb.MoveRotation (startRotation);
		rb.transform.localScale = startScale;
		transform.localScale = startScale;
	}

	public SerializableModel SerializableModel {
		get {
			if (serializableModel == null) {
				serializableModel = new SerializableModel (this);
			}
			return serializableModel;
		}
	}
	#endregion
}