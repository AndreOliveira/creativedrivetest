﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class LoadModels : MonoBehaviour {

	#region  properties
	[SerializeField] private string uri = "https://s3-sa-east-1.amazonaws.com/static-files-prod/unity3d/models.json";
	[SerializeField] private ScriptableSelectedModel selectedModel;
	[SerializeField] private ScriptableAppEvent onSelectedEvent;
	[SerializeField] private ScriptableAppEvent onAllModelsLoaded;
	[SerializeField] private GameObject LoadUI;

	private string filePath;
	private List<Model> allModels;
	public ModelsArray modelsArray { get; private set; }
	#endregion

	#region monobehaviour method
	private void Start () {
		//reset transform to avoid problems with childrens
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.localScale = Vector3.one;

		allModels = new List<Model> ();
		modelsArray = null;

		filePath = string.Format("{0}/{1}", Application.persistentDataPath, Constants.FILE_NAME);

		// 1 represents true, and 0 represents false
		if (PlayerPrefs.GetInt (Constants.LOAD_FILE_KEY) != 1) {
			StartCoroutine (DownloadJSON ());
		} else {
			string json = File.ReadAllText (filePath);
			modelsArray = JsonUtility.FromJson<ModelsArray> (json);
		}
		StartCoroutine (LoadAllModels ());
	}
	#endregion

#region private methods
	//TODO: treat when connection fail
	private IEnumerator LoadAllModels () {
		while (modelsArray == null) {
			yield return null;
		}

		for (int i = 0; i < modelsArray.models.Count; i++) {
			LoadModel (modelsArray.models[i]);
		}
		onAllModelsLoaded.Call ();
	}

	private void LoadModel (SerializableModel serializableModel) {
		//create object
		GameObject modelObject = CreateModel (serializableModel);
		Model model = modelObject.AddComponent<Model> ();
		allModels.Add (model);

		//setup transform
		SetupTransform (modelObject.transform, serializableModel);

		// setup model
		model.SetupModel (serializableModel, selectedModel);
		model.onSelectedEvent = onSelectedEvent;

		//setup material
		SetupMaterial (model.material, serializableModel);
	}

	private GameObject CreateModel (SerializableModel serializableModel) {
		//Instantiation
		GameObject fbx = Resources.Load (Constants.Path.FBX + serializableModel.name) as GameObject;
		GameObject modelObject = Instantiate (fbx);

		//Setup
		modelObject.name = serializableModel.name;
		modelObject.transform.parent = transform;
		modelObject.AddComponent<BoxCollider> ();

		return modelObject;
	}

	private void SetupTransform (Transform transform, SerializableModel serializableModel) {
		transform.position = serializableModel.GetPosition();
		transform.rotation = serializableModel.GetRotation();
		transform.localScale = serializableModel.GetScale();
	}

	private void SetupMaterial (Material mat, SerializableModel serializableModel) {
		//set albedo
		Texture albedo = Resources.Load<Texture> (Constants.Path.MAIN_TEXTURE + serializableModel.name);
		mat.SetTexture (Constants.ALBEDO_KEY, albedo);

		//set normalmap
		Texture normalMap = Resources.Load<Texture> (Constants.Path.NORMAL_MAP + serializableModel.name);
		mat.SetTexture (Constants.NORMAL_MAP_KEY, normalMap);
	}
	#endregion

	#region JSON methods
	private IEnumerator DownloadJSON () {
		UnityWebRequest webRequest = UnityWebRequest.Get (uri);
		yield return webRequest.SendWebRequest ();

		if (webRequest.isNetworkError) {
			//if some error occurs, start an empty scene
			modelsArray = new ModelsArray ();
			modelsArray.models = new List<SerializableModel> ();
		} else {
			int skipBOM = 3;
			byte[] data = webRequest.downloadHandler.data;
			string json = Encoding.UTF8.GetString (webRequest.downloadHandler.data, skipBOM, data.Length - skipBOM);
			modelsArray = JsonUtility.FromJson<ModelsArray> (json);
		}
	}

	public void SaveJSON () {
		modelsArray.models.Clear ();
		//update all models to save
		for (int i = 0; i < allModels.Count; i++) {
			SerializableModel serializableModel = allModels[i].SerializableModel;
			serializableModel.UpdateModel (allModels[i]);
			modelsArray.models.Add (serializableModel);
		}

		//convert models to json
		string json = JsonUtility.ToJson (modelsArray, true);

		//serialize json into file
		filePath = string.Format ("{0}/{1}", Application.persistentDataPath, Constants.FILE_NAME);
		File.WriteAllText (filePath, json);
		print ("Ajudar o avalador a encontrar o arquivo: " + filePath);
	}
	#endregion

	#region public methods
	public void AddDuplicatedModel () {
		GameObject duplicated = selectedModel.GetLastDuplicated ();
		duplicated.transform.parent = gameObject.transform;
		Model model = duplicated.GetComponent<Model> ();
		SerializableModel newModel = model.SerializableModel;
		modelsArray.models.Add (newModel);
		allModels.Add (model);
	}

	public void ReloadModels (bool loadFromFile) {
		int fromFiles = loadFromFile ? 1 : 0;
		PlayerPrefs.SetInt (Constants.LOAD_FILE_KEY, fromFiles);
		PlayerPrefs.Save ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (Constants.Scene.LOAD_SCREEN);
	}

	public void UpateLoadButton (UnityEngine.UI.Button loadFromFileButton) {
		loadFromFileButton.interactable = File.Exists (filePath);
	}
	#endregion
}