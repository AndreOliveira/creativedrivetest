﻿using System;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (fileName = "SelectedModel", menuName = "CreativeDriveTest/SelectedModel", order = 0)]
public class ScriptableSelectedModel : ScriptableObject, ISerializationCallbackReceiver {
	#region properties
	public Model model;
	[SerializeField] private ScriptableAppEvent onDuplicateModel;
	[NonSerializedAttribute] private GameObject duplicate;
	#endregion

	#region interface methods
	public void OnAfterDeserialize () {
		model = null;
	}

	//exception: this method is not used, interface used only to use "OnAfterDeserialize"
	public void OnBeforeSerialize () { }
	#endregion

	#region public methods
	public void Duplicate () {
		//setup game object
		GameObject clone = Instantiate (model.gameObject, model.transform.position, model.transform.rotation) as GameObject;
		clone.name = model.gameObject.name;

		//setup material
		Material newMaterial = new Material (model.GetComponent<Renderer> ().material);
		clone.GetComponent<Renderer> ().material = newMaterial;
		clone.GetComponent<Model> ().material = newMaterial;

		duplicate = clone;
		onDuplicateModel.Call ();
	}

	public void ChangeColor (int index) {
		model.ChangeColor (index);
	}

	public void ChangeTexture (int index) {
		model.ChangeTexture (index);
	}

	public void ResetModel () {
		model.ResetModel ();
	}

	public GameObject GetLastDuplicated () {
		return duplicate;
	}
	#endregion
}