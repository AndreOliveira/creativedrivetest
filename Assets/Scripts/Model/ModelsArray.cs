﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModelsArray {
	public List<SerializableModel> models;
}

[System.Serializable]
public class SerializableModel {

	#region properties
	public string name;
	public float[] position;
	public float[] rotation;
	public float[] scale;

	//a good idea is serialize this attributes
	[NonSerializedAttribute] public int selectedTexture = 0;
	[NonSerializedAttribute] public int selectedColor = 0;
	[NonSerializedAttribute] public Color[] colors = new Color[3] { Color.white, Color.red, Color.green };
	#endregion

	#region private methods
	private Vector3 FloatsToVector3 (float[] coords) {
		Vector3 vec3 = new Vector3 ();
		vec3.x = coords[0];
		vec3.y = coords[1];
		vec3.z = coords[2];
		return vec3;
	}

	private float[] Vector3ToFloatArray (Vector3 vector3) {
		return new float[] {
			vector3.x,
				vector3.y,
				vector3.z
		};
	}
	#endregion

	#region public methods
	public SerializableModel (Model model) {
		name = model.name;
		position = Vector3ToFloatArray (model.transform.position);
		rotation = Vector3ToFloatArray (model.transform.rotation.eulerAngles);
		scale = Vector3ToFloatArray (model.transform.localScale);
		selectedTexture = model.selectedTexture;
		selectedColor = model.selectedColor;
	}

	public SerializableModel () { }

	public void UpdateModel (Model model) {
		name = model.name;
		position = Vector3ToFloatArray (model.transform.position);
		rotation = Vector3ToFloatArray (model.transform.rotation.eulerAngles);
		scale = Vector3ToFloatArray (model.transform.localScale);
		selectedTexture = model.selectedTexture;
		selectedColor = model.selectedColor;
	}

	public Texture[] LoadTextures () {
		return new Texture[3] {
			Resources.Load (Constants.Path.MAIN_TEXTURE + name) as Texture,
				Resources.Load (Constants.Path.TEXTURE_ALT_1 + name) as Texture,
				Resources.Load (Constants.Path.TEXTURE_ALT_2 + name) as Texture
		};
	}

	public Vector3 GetPosition () {
		return FloatsToVector3 (position);
	}

	public Quaternion GetRotation () {
		return Quaternion.Euler (FloatsToVector3 (rotation));
	}

	public Vector3 GetScale () {
		return FloatsToVector3 (scale);
	}
	#endregion

}