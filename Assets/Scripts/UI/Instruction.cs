﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instruction : MonoBehaviour
{
	[SerializeField] private GameObject helpPanel;
	[SerializeField] private Text helpButtonText;

	public void EnableOrDisable(){
		helpPanel.SetActive(!helpPanel.activeSelf);
		helpButtonText.text = helpPanel.activeSelf ? "Esconder" : "Ver Instruções";
	}
}
