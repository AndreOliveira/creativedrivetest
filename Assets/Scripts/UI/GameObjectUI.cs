﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameObjectUI : MonoBehaviour {

	#region properties
	[SerializeField] private ScriptableSelectedModel selectedModel;
	[SerializeField] private Gizmo gizmo;
	[SerializeField] private Text displayName;

	[Header ("Position InputFields")]
	[SerializeField] private InputField positionX;
	[SerializeField] private InputField positionY;
	[SerializeField] private InputField positionZ;

	[Header ("Rotation InputFields")]
	[SerializeField] private InputField rotationX;
	[SerializeField] private InputField rotationY;
	[SerializeField] private InputField rotationZ;

	[Header ("Scale InputFields")]
	[SerializeField] private InputField scaleX;
	[SerializeField] private InputField scaleY;
	[SerializeField] private InputField scaleZ;
	#endregion

	#region Update UI
	public void UpdateGameObjectUI () {
		displayName.text = selectedModel.model.gameObject.name;
		UpdatePositionUI ();
		UpdateRotationUI ();
		UpdateScaleUI ();
	}

	public void UpdatePositionUI () {
		positionX.text = selectedModel.model.transform.position.x.ToString ();
		positionY.text = selectedModel.model.transform.position.y.ToString ();
		positionZ.text = selectedModel.model.transform.position.z.ToString ();
	}
	public void UpdateRotationUI () {
		rotationX.text = selectedModel.model.transform.rotation.eulerAngles.x.ToString ();
		rotationY.text = selectedModel.model.transform.rotation.eulerAngles.y.ToString ();
		rotationZ.text = selectedModel.model.transform.rotation.eulerAngles.z.ToString ();
	}
	public void UpdateScaleUI () {
		scaleX.text = selectedModel.model.transform.localScale.x.ToString ();
		scaleY.text = selectedModel.model.transform.localScale.y.ToString ();
		scaleZ.text = selectedModel.model.transform.localScale.z.ToString ();
	}
	#endregion

	#region update transform through UI
	public void UpdatePosition () {
		float x = float.Parse (positionX.text);
		float y = float.Parse (positionY.text);
		float z = float.Parse (positionZ.text);
		Vector3 pos = new Vector3 (x, y, z);
		gizmo.MovePosition (pos);
		selectedModel.model.rb.MovePosition (pos);
	}
	public void UpdateRotation () {
		float x = float.Parse (rotationX.text);
		float y = float.Parse (rotationY.text);
		float z = float.Parse (rotationZ.text);
		selectedModel.model.rb.MoveRotation (Quaternion.Euler (new Vector3 (x, y, z)));
	}
	public void UpdateScale () {
		float x = float.Parse (scaleX.text);
		float y = float.Parse (scaleY.text);
		float z = float.Parse (scaleZ.text);
		selectedModel.model.transform.localScale = new Vector3 (x, y, z);
	}

	public void ResetTransformUI() {
		//reset position
		positionX.text = selectedModel.model.startPosition.x.ToString();
		positionY.text = selectedModel.model.startPosition.y.ToString();
		positionZ.text = selectedModel.model.startPosition.z.ToString();
	
		//reset rotation
		rotationX.text = selectedModel.model.startRotation.eulerAngles.x.ToString();
		rotationY.text = selectedModel.model.startRotation.eulerAngles.y.ToString();
		rotationZ.text = selectedModel.model.startRotation.eulerAngles.z.ToString();

		//reset scale
		scaleX.text = selectedModel.model.startScale.x.ToString();
		scaleY.text = selectedModel.model.startScale.y.ToString();
		scaleZ.text = selectedModel.model.startScale.z.ToString();
	}
	#endregion
}