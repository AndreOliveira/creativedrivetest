﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThumbnailManager : MonoBehaviour {

	#region properties
	public GameObject thumbnailPrefab;
	public List<Thumbnail> thumbnails;
	public RectTransform panel;
	public float enlargePanelFactor = 110f;
	public float smooth;
	private float vel;
	private float startWidth = 10f;
	private int maxThumbnails = 10;
	#endregion

	private void Start () {
		vel = 0;
	}

	private void CreateThumbnail (string thumbnailName) {
		//create thumbnail
		GameObject thumbnailGameObject = Instantiate(thumbnailPrefab);
		thumbnailGameObject.transform.SetParent(transform);

		//setupt anchor and pivot
		RectTransform rt = thumbnailGameObject.GetComponent<RectTransform> ();
		SetupAnchorsAndPivotToRight (rt);

		//setup transform
		float x = -startWidth - enlargePanelFactor * thumbnails.Count;
		Vector3 position = new Vector3 (x, startWidth, 0f);
		rt.localPosition = position;
		rt.localScale = Vector3.one;

		//setup thumbnail
		Thumbnail thumbnail = thumbnailGameObject.GetComponent<Thumbnail>();
		thumbnail.modelName = thumbnailName;
		thumbnail.LoadThumbnailImage();

		thumbnails.Add (thumbnail);
		StartCoroutine (EnlargePanel ());
	}

	private IEnumerator EnlargePanel () {
		//enlarge factor
		float endWidth = startWidth + enlargePanelFactor * (thumbnails.Count);
		float epsilon = 1f;

		while (panel.rect.width < endWidth - epsilon) {
			Vector2 size = panel.sizeDelta;
			size.x = Mathf.SmoothDamp (size.x, endWidth, ref vel, smooth);
			panel.sizeDelta = size;
			yield return new WaitForFixedUpdate ();
		}
	}

	private void SetupAnchorsAndPivotToRight (RectTransform rectT) {
		rectT.anchorMin = Vector2.right;
		rectT.anchorMax = Vector2.right;
		rectT.pivot = Vector2.right;
	}

	public void AddAllThumbnails (LoadModels loadModels) {
		SerializableModel[] models = loadModels.modelsArray.models.ToArray();

		//add all thumbnails
		for (int i = 0; i < models.Length; i++) {
			AddThumbnail (models[i].name);
		}
	}

	public void AddThumbnail (string thumbnailName) {
		for (int i = 0; i < thumbnails.Count; i++) {
			if (thumbnails[i].modelName == thumbnailName) {
				thumbnails[i].Amount++;
				return;
			}
		}

		// verify if is possible create a new thumbnail
		if (thumbnails.Count >= maxThumbnails) return;
		CreateThumbnail (thumbnailName);
	}

	public void AddThumbnail (ScriptableSelectedModel selectedModel) {
		AddThumbnail(selectedModel.model.name);
	}
}