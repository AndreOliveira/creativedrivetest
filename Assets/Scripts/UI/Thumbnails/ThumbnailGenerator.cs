﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ThumbnailGenerator : MonoBehaviour {

	#region properties
	[SerializeField] private Camera cam;
	[SerializeField] private Shader shader;
	[SerializeField] private GameObject[] models;

	private string thumbnailDirectory;
	private bool generateThumbnail;
	private int thumbnailIndex;
	#endregion

	#region Monobehaviour method
	private void Start () {
		thumbnailIndex = 0;
		generateThumbnail = false;

		thumbnailDirectory = string.Format ("{0}/Resources/{1}", Application.dataPath, Constants.Path.THUMBNAIL);

		//setup models
		for (int i = 0; i < models.Length; i++)
		{
			Material mat = models[i].GetComponent<Renderer>().material = new Material(shader);
			mat.SetTexture(Constants.ALBEDO_KEY, Resources.Load<Texture>(Constants.Path.MAIN_TEXTURE + models[i].name));
			mat.SetTexture(Constants.NORMAL_MAP_KEY, Resources.Load<Texture>(Constants.Path.NORMAL_MAP + models[i].name));
		}

		//create thumbnail directory
		if (!Directory.Exists (thumbnailDirectory)) {
			Directory.CreateDirectory (thumbnailDirectory);
		}
	}

	private void OnPostRender () {
		if (generateThumbnail) {
			GenerateThumbnail (thumbnailIndex);
			generateThumbnail = false;
		}
	}
	#endregion

	#region Thumbnail generation
	private void GenerateThumbnail (int modelIndex) {
		//create texture and prepare camera
		Texture2D thumbnail = new Texture2D (cam.targetTexture.width, cam.targetTexture.height, TextureFormat.ARGB32, false);
		RenderTexture.active = cam.targetTexture;
		cam.Render ();

		// take a 'photo' and generate the thumbnail
		thumbnail.ReadPixels (new Rect (0f, 0f, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
		byte[] bytes = thumbnail.EncodeToPNG ();
		string filePath = string.Format ("{0}{1}.png", thumbnailDirectory, models[modelIndex].name);
		File.WriteAllBytes (filePath, bytes);
	}

	private IEnumerator GenerateAllThumbnails () {
		//prepare to generate
		SetModelsActivation (false);
		thumbnailIndex = 0;

		for (int i = 0; i < models.Length; i++) {
			thumbnailIndex = i;
			models[i].SetActive (true);

			//wait the object be enabled to capture the image
			yield return new WaitUntil(() => models[i].activeSelf);
			generateThumbnail = true;

			//wait image be captured to disable the object
			yield return new WaitUntil(() => !generateThumbnail);
			models[thumbnailIndex].SetActive (false);
		}
		//reactive all models
		SetModelsActivation(true);
	}

	public void GenerateThumbnails () {
		StartCoroutine (GenerateAllThumbnails ());
	}

	private void SetModelsActivation (bool active) {
		for (int i = 0; i < models.Length; i++) {
			models[i].SetActive (active);
		}
	}
	#endregion
}