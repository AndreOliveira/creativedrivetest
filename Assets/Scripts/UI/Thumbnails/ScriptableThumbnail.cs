﻿using UnityEngine;

[CreateAssetMenu(fileName = "Thumbnail", menuName = "CreativeDriveTest/Thumbnail", order = 2)]
public class ScriptableThumbnail : ScriptableObject {}