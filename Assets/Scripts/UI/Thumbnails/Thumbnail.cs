﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Thumbnail : MonoBehaviour {
	
	[SerializeField] private Text amountText;
	[SerializeField] private Image thumbnail;
	private int amount = 1;
	public string modelName;


	private void Start () {
		amountText.text = amount.ToString ();
	}

	public void LoadThumbnailImage () {
		Sprite sprite = Resources.Load<Sprite> (Constants.Path.THUMBNAIL + modelName);
		thumbnail.sprite = sprite;
	}

	public int Amount {
		get { return amount; }
		set {
			amount = value;
			amountText.text = amount.ToString ();
		}
	}
}