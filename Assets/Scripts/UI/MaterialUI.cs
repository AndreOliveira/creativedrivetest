﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialUI : MonoBehaviour {
	#region properties
	[SerializeField] private ScriptableSelectedModel selectedModel;

	[Header ("Textures Highlight")]
	[SerializeField] private Image[] texturesHighlights;

	[Header ("Textures")]
	[SerializeField] private int textureIndex = 0;
	[SerializeField] private RawImage[] texturesUI;

	[Header ("Colors")]
	[SerializeField] private Image[] colorsUI;

	[Header ("Colors Highlight")]
	[SerializeField] private int colorIndex = 0;
	[SerializeField] private Image[] colorsHighlights;
	#endregion

	#region Update UI
	private void UpdateTexturesToUI () {
		for (int i = 0; i < texturesUI.Length; i++) {
			texturesUI[i].texture = selectedModel.model.textures[i];
			texturesHighlights[i].enabled = selectedModel.model.selectedTexture == i;
		}
	}

	private void UpdateColorsToUI () {
		for (int i = 0; i < colorsUI.Length; i++) {
			colorsUI[i].color = selectedModel.model.colors[i];
			colorsHighlights[i].enabled = selectedModel.model.selectedColor == i;
		}
	}

	public void UpdateMaterialUI () {
		UpdateColorsToUI ();
		UpdateTexturesToUI ();
	}
	#endregion

	#region change material fields
	public void ChangeColor (int index) {
		colorIndex = index;
		UpdateColorsToUI ();
	}

	public void ChangeTexture (int index) {
		textureIndex = index;
		UpdateTexturesToUI ();
	}
	#endregion
}