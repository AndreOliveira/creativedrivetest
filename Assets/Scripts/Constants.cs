﻿public static class Constants {

	public static readonly string ALBEDO_KEY = "_MainTex";
	public static readonly string ALBEDO_COLOR_KEY = "_Color";
	public static readonly string FILE_NAME = "models.json";
	public static readonly string NORMAL_MAP_KEY = "_BumpMap";
	public static readonly string LOAD_FILE_KEY = "LoadFromFile";

	public static readonly float DEFAULT_MULTIPLIER = 10f;

	public static class Path {
		public static readonly string THUMBNAIL = "Thumbnails/";
		public static readonly string FBX = "FBX/";
		public static readonly string MAIN_TEXTURE = "Textures/Albedo/texture-albedo-";
		public static readonly string TEXTURE_ALT_1 = MAIN_TEXTURE + "alt01-";
		public static readonly string TEXTURE_ALT_2 = MAIN_TEXTURE + "alt02-";
		public static readonly string NORMAL_MAP = "Textures/NormalMap/texture-normalmap-";
	}

	public static class Scene {
		public static readonly string LOAD_SCREEN = "LoadScreen";
	}

}