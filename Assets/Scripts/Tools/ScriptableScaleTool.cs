﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScaleTool", menuName = "TransformTools/ScaleTool", order = 2)]
public class ScriptableScaleTool : AbstractScriptableTool {
	
	//change scale when the user uses mouse scroll
	public override bool CheckUse(Camera cam){
		float scrollFactor = Input.mouseScrollDelta.y * Time.deltaTime * mouseSensibility;

		//if dont use scroll, stops here or it will update very fast and
		//make impossible change scale through 'menu'
		if (scrollFactor == 0) return false;

		selectedModel.model.transform.localScale += Vector3.one * scrollFactor;
		toolEvent.Call ();
		return false;
	}
	
	//this is an exception. I choose use this scriptable object to maintain the pattern
	//what is used to others tools (tranlation and rotation tools). In this case, this
	//method is not used and is useless
	public override IEnumerator Use(Rigidbody rbGizmo){
		yield return null;
	}
}