﻿using System;
using System.Collections;
using UnityEngine;

public abstract class AbstractScriptableTool : ScriptableObject {

	#region properties
	[SerializeField] protected ScriptableAppEvent toolEvent;
	[SerializeField] protected ScriptableSelectedModel selectedModel;
	[SerializeField] protected LayerMask layerMask;
	[SerializeField] protected float mouseSensibility;
	
	[NonSerializedAttribute] protected DragAxis.Axis currentDragAxis;
	protected bool isInteracting;
	protected Coroutine interactionCR;
	#endregion

	public abstract bool CheckUse(Camera cam);
	public abstract IEnumerator Use(Rigidbody rbGizmo);

	protected bool TrySelect (Camera cam) {
		//setup raycast
		Ray ray = cam.ScreenPointToRay (Input.mousePosition);
		float distance = 1000f;
		RaycastHit hit;

		//verify what axis was clicked
		if (Physics.Raycast (ray, out hit, distance, layerMask)) {
			currentDragAxis = hit.collider.GetComponent<DragAxis> ().axis;
			return true;
		}
		return false;
	}
}