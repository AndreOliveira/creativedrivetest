﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TranslationTool", menuName = "TransformTools/TranslationTool", order = 0)]
public class ScriptableTranslationTool : AbstractScriptableTool {
	
	public override bool CheckUse(Camera cam){
		if (Input.GetMouseButtonDown (0) && !isInteracting) {
			isInteracting = true;
			return base.TrySelect(cam);
		}
		if (Input.GetMouseButtonUp(0)) {
			isInteracting = false;
		}
		return false;
	}
	
	//change X, Y or Z position
	public override IEnumerator Use(Rigidbody rbGizmo){
		Rigidbody rbModel = selectedModel.model.rb;
		while (isInteracting) {
			Vector3 newPos;
			switch (currentDragAxis) {
				case DragAxis.Axis.X:
					newPos = rbModel.position + Vector3.right * Input.GetAxis ("Mouse X") * mouseSensibility;
					rbModel.MovePosition (newPos);
					rbGizmo.MovePosition (newPos);
					break;
				case DragAxis.Axis.Y:
					newPos = rbModel.position + Vector3.up * Input.GetAxis ("Mouse Y") * mouseSensibility;
					rbModel.MovePosition (newPos);
					rbGizmo.MovePosition (newPos);
					break;
				case DragAxis.Axis.Z:
					newPos = rbModel.position + Vector3.forward * Input.GetAxis ("Mouse Y") * mouseSensibility;
					rbModel.MovePosition (newPos);
					rbGizmo.MovePosition (newPos);
					break;
			}
			toolEvent.Call ();
			yield return new WaitForFixedUpdate ();
		}
	}
}