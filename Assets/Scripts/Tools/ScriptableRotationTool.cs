﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "RotationTool", menuName = "TransformTools/RotationTool", order = 1)]
public class ScriptableRotationTool : AbstractScriptableTool {

	public override bool CheckUse(Camera cam){
		if (Input.GetMouseButtonDown (0) && !isInteracting) {
			isInteracting = true;
			return base.TrySelect(cam);
		}
		if (Input.GetMouseButtonUp(0)) {
			isInteracting = false;
		}
		return false;
	}

	//rotate around the X, Y or Z axis
	public override IEnumerator Use (Rigidbody rbGizmo) {
		while (isInteracting) {
			Vector3 rot = selectedModel.model.rb.rotation.eulerAngles;
			switch (currentDragAxis) {
				case DragAxis.Axis.X:
					rot += Vector3.right * Input.GetAxis("Mouse Y") * mouseSensibility * Constants.DEFAULT_MULTIPLIER;
					selectedModel.model.rb.MoveRotation (Quaternion.Euler (rot));
					break;
				case DragAxis.Axis.Y:
					rot += Vector3.up * Input.GetAxis("Mouse X") * mouseSensibility * Constants.DEFAULT_MULTIPLIER;
					selectedModel.model.rb.MoveRotation (Quaternion.Euler (rot));
					break;
				case DragAxis.Axis.Z:
					rot += Vector3.forward * Input.GetAxis("Mouse X") * mouseSensibility * Constants.DEFAULT_MULTIPLIER;
					selectedModel.model.rb.MoveRotation (Quaternion.Euler (rot));
					break;
			}
			toolEvent.Call ();
			yield return new WaitForFixedUpdate ();
		}
	}
}