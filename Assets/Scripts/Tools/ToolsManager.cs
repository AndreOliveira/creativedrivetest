﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolsManager : MonoBehaviour {
	#region properties
	[SerializeField] private AbstractScriptableTool currentTool;
	[SerializeField] private AbstractScriptableTool alwaysActiveTool;
	[SerializeField] private Camera cam;
	[SerializeField] private Rigidbody gizmoRB;
	[SerializeField] private ScriptableSelectedModel selectedModel;
	#endregion

	private void Update () {
		if (!selectedModel.model) {
			return;
		}
		if (currentTool && currentTool.CheckUse (cam)) {
			StartCoroutine (currentTool.Use (gizmoRB));
		}
		alwaysActiveTool.CheckUse (cam);
	}

	public void SetTool (AbstractScriptableTool tool) {
		currentTool = tool;
	}

}