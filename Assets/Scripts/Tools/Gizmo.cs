﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class Gizmo : MonoBehaviour {

	#region properties
	[SerializeField] private ScriptableSelectedModel selectedModel;
	[SerializeField] Renderer[] childrenRenderers;
	private Collider[] childrenColliders;
	private Rigidbody rb;
	#endregion

	#region monobehaviour methods
	private void Awake () {
		rb = GetComponent<Rigidbody> ();
	}

	private void Start () {
		childrenColliders = GetComponentsInChildren<Collider> ();
		SetActiveChildrenFunctions (false);
	}
	#endregion

	#region private methods
	private void SetActiveChildrenFunctions (bool active) {
		for (int i = 0; i < childrenRenderers.Length; i++) {
			childrenRenderers[i].enabled = active;
		}
		for (int i = 0; i < childrenColliders.Length; i++) {
			childrenColliders[i].enabled = active;
		}
	}

	private IEnumerator ActiveGizmo () {
		//set gizmo transform
		Transform model = selectedModel.model.gameObject.transform;
		rb.MovePosition (model.position);

		//set child and parent
		yield return new WaitForFixedUpdate ();

		//active gizmos renders
		SetActiveChildrenFunctions (true);
	}
	#endregion

	#region public methods
	public void Active () {
		StartCoroutine (ActiveGizmo ());
	}

	public void MovePosition (Vector3 position) {
		rb.MovePosition (position);
	}

	public void ResetPosition () {
		rb.MovePosition (selectedModel.model.startPosition);
	}
	#endregion
}