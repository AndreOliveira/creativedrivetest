﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "AppEvent", menuName = "CreativeDriveTest/AppEvent", order = 1)]
public class ScriptableAppEvent : ScriptableObject {
	private List<AppEventListener> listeners = new List<AppEventListener> ();

	public void Call () {
		for (int i = 0; i < listeners.Count; i++) {
			listeners[i].OnEventCalled ();
		}
	}

	public void AddListener(AppEventListener listener) {
		listeners.Add(listener);
	}
	public void RemoveListener(AppEventListener listener) {
		listeners.Remove(listener);
	}
}