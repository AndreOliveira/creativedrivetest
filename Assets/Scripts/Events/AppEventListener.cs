﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AppEventListener : MonoBehaviour
{
	public ScriptableAppEvent appEvent;
	public UnityEvent response;

    public void OnEventCalled()
    {
        response.Invoke();
    }

	private void OnDisable() {
		appEvent.RemoveListener(this);
	}

	private void OnEnable() {
		appEvent.AddListener(this);
	}
}
